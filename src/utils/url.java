package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class url {
	
	/**
	 * 使用url进行模拟登陆
	 */
	static String charset = "utf-8";
	static String result = "";
	/**
	 * 
	 * @param url
	 * @param param
	 * @return 网页内容
	 */
	public static String sendURLPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			URL readUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) readUrl.openConnection();
			// conn.setRequestProperty(key, value);
			conn.setRequestProperty("Accept-Charset", charset);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
			// 发送POST请求必须设置如下两行
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// conn.setR
			conn.connect();
			// param = URLEncoder.encode(param, "UTF-8");
			out = new PrintWriter(conn.getOutputStream());
			out.print(param);
			System.out.println("param: " + param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				line = new String(line.getBytes(), "UTF-8");
				result += line;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return result;
	}

}
