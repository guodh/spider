package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
 

public class XinLangDao {

	Properties po = new Properties();
		
	public Connection getConnect(){
		try {
			po.load(new FileInputStream(new File("db.properties")));
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=root&password=root&useUnicode=true&characterEncoding=UTF-8");
			return connection;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("数据库连接出现问题");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("数据库连接出现问题");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 插入数据
	 * @param content
	 */
	public void insertDb(String content){
		String insertSql = "INSERT INTO XinLang(content) VALUES (?)";
		PreparedStatement ps;
		try {
			ps = getConnect().prepareStatement(insertSql);
			ps.setString(1, content);
			ps.executeUpdate();
			ps.close();
			getConnect().close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("执行insert出现问题");
		}
	}
	
	/**
	 * 查询是否存在
	 * @param content
	 * @return 不存在返回-1
	 */
	public int select(String content){
		String selectSql = "SELECT * from XinLang WHERE content= ?";
		PreparedStatement ps;
		int row = -1;
		try {
			ps = getConnect().prepareStatement(selectSql);
			ps.setString(1, content);
			ps.executeQuery();
			ResultSet resultSet = ps.getResultSet();
			if(resultSet.next()){
				row = Integer.parseInt(resultSet.getString(1));
			}else{
				row = -1;
				System.out.println("****没有相同的内容存在数据库中*****");
			}
			//ps.
			ps.close();
			getConnect().close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("执行select出现问题");
		}
		return row;
	}
	
}
