package test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.XinLangDao;

public class mytest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	public String dealData(String text) {
		String string = null;
		Document doc = Jsoup.parse(text);
		Elements element = doc.select("script");
		string = element.toString().replace("\\n", "").replace("\\r", "").replace("\\t", "").replace("\\", "")
				.replace("<script>FM.view({\"ns\":\"pl.content.homeFeed.index", "") + " ";
		return string;
	}

	public static String finalDeal(String text) throws UnsupportedEncodingException, IOException {
		String string = null;
		StringBuilder sb = new StringBuilder();
		Document doc = Jsoup.parse(text);
		// writeFile("text.html", doc.toString(), "GB2312");
		// 获取标题
		XinLangDao xinLangDao = new XinLangDao();
		Elements select = doc.select("div.WB_detail");
		for (Element element : select) {
			System.out.println("进入foreach");
			System.out.println(element.text());
			// xinLangDao.insertDb(element.text());
			// System.out.println("!!!!");
			int row = xinLangDao.select(element.text());
			System.out.println(row);
		}

		System.out.println("end");
		return string;
	}

	public static void writeFile(String filepath, String value, String encoding)
			throws UnsupportedEncodingException, IOException {
		File file = new File(filepath);
		FileOutputStream fos = null;
		fos = new FileOutputStream(file, false);
		fos.write(value.getBytes(encoding));
		fos.close();
	}

	@Test
	public void test() {
		File file = new File("result.txt");
		String len;
		String str = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((len = br.readLine()) != null) {
				str = str + len;
			}
			str = dealData(str);
			finalDeal(str);
			System.out.println("finalDeal(str) is end");
			Pattern p = Pattern.compile("mid=(.*?)class");
			Matcher m = p.matcher(str);
			List<String> result = new ArrayList<String>();
			while (m.find()) {
				String m_group = m.group();
				if (m_group.length() < 80) {
					result.add(m_group);
				}
			}
			for (String s1 : result) {
				// System.out.println(s1);
			}
			System.out.println("****end****");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
