package SunnyRun;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SunnySport {

	final String url = "http://hdu.sunnysport.org.cn";
	private String name;
	private String password;
	private static CloseableHttpClient httpClient = HttpClients.createDefault();
	private static HttpClientContext context = new HttpClientContext();
	private CloseableHttpResponse response = null;

	public SunnySport(String name) {
		this.name = name;
		this.password = name;
	}

	public SunnySport(String name, String pasword) {
		this.name = name;
		this.password = pasword;
	}
	/**
	 * 开始执行
	 */
	public void run() {
		
		sendMessage();
		analysis();
	}

	public boolean sendMessage() {

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("username", name));
		nvps.add(new BasicNameValuePair("password", password));
		HttpPost post = new HttpPost(url);

		// nvps是包装请求参数的list

		try {
			if (nvps != null) {
				post.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

			}
			// 执行请求用execute方法，content用来帮我们附带上额外信息
			response = httpClient.execute(post, context);
			// 看是否重定向，并测试
			System.out.println(response.getStatusLine().getStatusCode());
			Header firstHeader = response.getFirstHeader("Location");
			System.out.println("name:" + firstHeader.getName());
			System.out.println("value:" + firstHeader.getValue());
			if (firstHeader.getValue() != null) {

				System.out.println("开始重定向=======");
				HttpPost post2 = new HttpPost(firstHeader.getValue());
				// nvps是包装请求参数的list
				if (nvps != null) {
					post2.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
				}
				// 执行请求用execute方法，content用来帮我们附带上额外信息
				response = httpClient.execute(post2, context);

				System.out.println(response.getStatusLine().getStatusCode());
				Header firstHeader2 = response.getFirstHeader("Location");
				return true;
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void analysis() {

		HttpEntity entity = response.getEntity();

		String content;
		try {
			content = EntityUtils.toString(entity);

			// 关闭输入流
			EntityUtils.consume(entity);
			// 使用jsoup 解析
			Document document = Jsoup.parse(content);
			Elements select = document.select("label");
			for (Element element : select) {
				String str = element.text();
				if (str != null && !str.equals("")) {
					System.out.println(str);
				}
			}
			Elements table_select = document.select("table");
			for (Element element : table_select) {
				Elements td_select = element.select("td");
				for (Element element2 : td_select) {
					System.out.println(element2.text());
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
