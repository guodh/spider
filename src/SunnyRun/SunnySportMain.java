package SunnyRun;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.HttpUtils;

public class SunnySportMain {

	static Logger logger = LogManager.getLogger(SunnySportMain.class);
	
	static String charset = "utf-8";
	static String result = "";
	private static CloseableHttpClient httpClient = HttpClients.createDefault();
	private static HttpClientContext context = new HttpClientContext();

	public static void main(String[] args) {

		String url = "http://hdu.sunnysport.org.cn";
		String param = "username=14031601&password=14031601";
		// System.out.println(sendURLPost(url, param));
		CloseableHttpResponse response = null;
		String content = null;
		/**
		 * 使用httpclient
		 */
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("username", "14031601"));
		nvps.add(new BasicNameValuePair("password", "14031601"));
		
		try {
			// HttpClient中的post请求包装类
			HttpPost post = new HttpPost(url);
			// nvps是包装请求参数的list
			if (nvps != null) {
				post.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			}
			// 执行请求用execute方法，content用来帮我们附带上额外信息
			response = httpClient.execute(post, context);
			// 看是否重定向，并测试
			System.out.println(response.getStatusLine().getStatusCode());
			Header firstHeader = response.getFirstHeader("Location");
			System.out.println("name:" + firstHeader.getName());
			System.out.println("value:" + firstHeader.getValue());
			if (firstHeader.getValue() != null) {

				System.out.println("开始重定向=======");
				HttpPost post2 = new HttpPost(firstHeader.getValue());
				// nvps是包装请求参数的list
				if (nvps != null) {
					post2.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
				}
				// 执行请求用execute方法，content用来帮我们附带上额外信息
				response = httpClient.execute(post2, context);

				System.out.println(response.getStatusLine().getStatusCode());
				Header firstHeader2 = response.getFirstHeader("Location");
			}
			// 得到相应实体、包括响应头以及相应内容
			HttpEntity entity = response.getEntity();

			content = EntityUtils.toString(entity);
			// 关闭输入流
			EntityUtils.consume(entity);
			// 使用jsoup 解析
			Document document = Jsoup.parse(content);
			Elements select = document.select("label");
			for (Element element : select) {
				String str = element.text();
				if (str != null && !str.equals("")) {
					System.out.println(str);
				}
				logger.info(str);
			}
			Elements table_select = document.select("table");
			for (Element element : table_select) {
				Elements td_select = element.select("td");
				for (Element element2 : td_select) {
					System.out.println(element2.text());
					logger.info(element2.text());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				//System.out.println(content);
			}
		}
	}
}
