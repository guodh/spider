package XinLangSpider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.DriverManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import dao.XinLangDao;

public class Handle {
	/**
	 * 
	 * @param 新浪的由script生成的html
	 * @return 处理后的string
	 */
	public  String dealData(String text) {
		String string = null;
		Document doc = Jsoup.parse(text);
		Elements element = doc.select("script");
		string = element.toString().replace("\\n", "").replace("\\r", "").replace("\\t", "").replace("\\", "")
				.replace("<script>FM.view({\"ns\":\"pl.content.homeFeed.index", "") + " ";
		return string;
	}

	/**
	 * 决定抽取的规则
	 * @param text
	 * @return 
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public String finalDeal(String text) throws UnsupportedEncodingException, IOException {
		String string = null;
		text = dealData(text);
		Document doc = Jsoup.parse(text);
		//writeFile("text2.html", doc.toString(), "GBK");  把处理好的html写出来看一下
		Elements select = doc.select("div.WB_detail");
		if(select.isEmpty()){
			System.out.println("网页内容为空，可能原因：cookies失效");
		}
		/**
		 * 存入数据库
		 */
		for (Element element : select) {
			XinLangDao dao = new XinLangDao();
			System.out.println(element.text());
			String content = element.text();
			if(dao.select(content) == -1){
				dao.insertDb(content);
				System.out.println("****有新的内容插入****");
			};
		}
		System.out.println("Jsoup分析完毕");
		return string;
	}

	public  void writeFile(String filepath, String value, String encoding)
			throws UnsupportedEncodingException, IOException {
		File file = new File(filepath);
		FileOutputStream fos = null;
		fos = new FileOutputStream(file, false);
		fos.write(value.getBytes(encoding));
		fos.close();
	}

}
