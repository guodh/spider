package XinLangSpider;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

public class MySpider implements PageProcessor {

	Map<String, String> map = new HashMap<String, String>();
	static Site site = new Site().me().setSleepTime(1000);

	public void process(Page page) {
		String text = page.getHtml().toString();
		Handle handle = new Handle();
		try {
			double start = System.currentTimeMillis();
			System.out.println("开始处理，存储网页内容");
			handle.finalDeal(text);
			System.out.println("处理结束，用时：" + (System.currentTimeMillis() - start));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Site getSite() {
		Properties pro = new Properties();
		try {
			pro.load(new FileInputStream(new File("cookies.properties")));
			for (Map.Entry<Object, Object> ement : pro.entrySet()) {
				site.addCookie((String) ement.getKey(), (String) ement.getValue());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return site;
	}

	public static void main(String[] args) {
		Spider.create(new MySpider()).addUrl("http://weibo.com/mukewang").run();
	}

}
